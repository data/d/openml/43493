# OpenML dataset: GoodReads-Choice-Awards

https://www.openml.org/d/43493

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

GoodReads Choice Awards
Every year GoodReads announces a contest for the best book of the year called GoodReads Choice Awards. Every year there are multiple categories and every reader can vote for a single book in each of them.
Data

are scraped from GoodReads Choice Awards awards websites using Python

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43493) of an [OpenML dataset](https://www.openml.org/d/43493). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43493/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43493/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43493/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

